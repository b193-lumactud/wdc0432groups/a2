package com.zuitt;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
public class PrimeNumbers {
    public static void main (String[] args){
        Scanner primeNumber = new Scanner(System.in);
        int pnumArray[] = new int[5];

        for (int i=0; i < pnumArray.length; i++) {
            System.out.print(pnumArray[i]+"\t");
        }

        System.out.println("Enter "+ pnumArray.length
                + " integer values:");

        for(int i=0; i < pnumArray.length; i++) {
            // read input
            pnumArray[i] = primeNumber.nextInt();
        }

        System.out.println("The first prime number is: "+pnumArray[1]);

        ArrayList<String> friends = new ArrayList<>();
        friends.add("Lourdes");
        friends.add("Rona");
        friends.add("Cristy");
        friends.add("Jezreel");

        System.out.println("My friends are: " +friends+"\n");

        HashMap<String, Integer> inventoryStocks = new HashMap<>();
        inventoryStocks.put ("Shampoo",12);
        inventoryStocks.put ("Conditioner", 11);
        inventoryStocks.put ("Soap",50);

        System.out.println("Our current inventory consist of: "+inventoryStocks+"\n");
    }
}
